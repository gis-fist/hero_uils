/**
 * 自动刷食物buff脚本
 * @param domain 你所在区url的域名代码，比如天涯区是z104
 * @param foodId 食物的编号，先拖一个食物到头像框里，在捕获的请求中找到"&id="后面的那串数字
 * @param foodAmount 背包中食物的个数
 */
function eat(domain, foodId, foodAmount) {
    if (foodAmount <= 0) {
        return;
    }
    setTimeout(() => {
        let curTime=new Date().getTime();
        fetch(`http://${domain}.hero.9wee.com/modules/role_item.php?act=drag_item&id=${foodId}&from=pack&to=none&op=use_to_role&timeStamp=${curTime}&callback_func_name=itemClass.dragItemCallback`)
            .then(() =>null);
        eat(domain, foodId, foodAmount - 1);
    }, 100);
}

//开始执行
//吃18个一口香
eat('z104', 34880937, 18);
